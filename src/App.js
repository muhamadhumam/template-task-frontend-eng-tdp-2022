
import banner from 'assets/img/architect.jpg';
import map from 'assets/img/map.jpg';
import 'assets/css/style.css';
import 'assets/css/custom.css';

const App = () => {
    return (
        <>
            <div className="top" >
                <div className="bar white wide padding card">
                    <a href="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</a>
                    <div className="right hide-small">
                        <a href="/" className="bar-item button">Home</a>
                        <a href="/shop" className="bar-item button">Shop</a>
                        <a href="/cart" className="bar-item button">My Cart</a>
                        <a href="/profile" className="bar-item button">My Profile</a>
                        <a href="/login" className="bar-item button">Login</a>
                    </div>
                </div>
            </div>
            <header className="display-container content wide" style={{ maxWidth: '1500px' }} id="home">
                <img className="image" src={banner} alt="Architecture" width={1500} height={800} />
                <div className="display-middle center">
                    <h1 className="xxlarge text-white">
                        <span className="padding black opacity-min">
                            <b>EDTS</b>
                        </span>
                        <span className="hide-small text-light-grey">Mart</span>
                    </h1>
                </div>
            </header>

            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="projects">
                    <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
                </div>
                <div className="row-padding">
                    <div className="col l3 m6 margin-bottom">
                        <a href={`/shop?category=1`}>
                            <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                                <div className="display-topleft black padding">Electronic</div>
                                <img src="https://fakestoreapi.com/img/81QpkIctqPL._AC_SX679_.jpg" alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
                            </div>
                        </a>
                    </div>
                    <div className="col l3 m6 margin-bottom">
                        <a href={`/shop?category=2`}>
                            <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                                <div className="display-topleft black padding">Jewelery</div>
                                <img src="https://fakestoreapi.com/img/71YAIFU48IL._AC_UL640_QL65_ML3_.jpg" alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
                            </div>
                        </a>
                    </div>
                    <div className="col l3 m6 margin-bottom">
                        <a href={`/shop?category=3`}>
                            <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                                <div className="display-topleft black padding">Men's Clothing</div>
                                <img src="https://fakestoreapi.com/img/71li-ujtlUL._AC_UX679_.jpg" alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
                            </div>
                        </a>
                    </div>
                    <div className="col l3 m6 margin-bottom">
                        <a href={`/shop?category=4`}>
                            <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                                <div className="display-topleft black padding">Handphone</div>
                                <img src="https://images.tokopedia.net/img/cache/500-square/VqbcmM/2021/11/18/46ad54a6-61e8-4939-9406-cc33d23b0346.jpg.webp?ect=4g" alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
                            </div>
                        </a>
                    </div>
                </div>

                <div className="container padding-32">
                    <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
                </div>
            </div>

            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="about">
                    <h3 className="border-bottom border-light-grey padding-16">
                        All Product
                    </h3>
                </div>
                <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                    <div className="product-card">
                        <div className="badge">Discount</div>
                        <div className="product-tumb">
                            <img src="https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg" alt="product1" />
                        </div>
                        <div className="product-details">
                            <span className="product-catagory">Men's Clothing</span>
                            <h4>
                                <a href={`/shop/1`}>Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops</a>
                            </h4>
                            <p>Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday</p>
                            <div className="product-bottom-details">
                                <div className="product-price">Rp. 250.000</div>
                                <div className="product-links">
                                    <a href={`/shop/1`}>View Detail<i className="fa fa-heart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="product-card">
                        <div className="badge">Discount</div>
                        <div className="product-tumb">
                            <img src="https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg" alt="product1" />
                        </div>
                        <div className="product-details">
                            <span className="product-catagory">Men's Clothing</span>
                            <h4>
                                <a href={`/shop/1`}>Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops</a>
                            </h4>
                            <p>Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday</p>
                            <div className="product-bottom-details">
                                <div className="product-price">Rp. 250.000</div>
                                <div className="product-links">
                                    <a href={`/shop/1`}>View Detail<i className="fa fa-heart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="product-card">
                        <div className="badge">Discount</div>
                        <div className="product-tumb">
                            <img src="https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg" alt="product1" />
                        </div>
                        <div className="product-details">
                            <span className="product-catagory">Men's Clothing</span>
                            <h4>
                                <a href={`/shop/1`}>Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops</a>
                            </h4>
                            <p>Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday</p>
                            <div className="product-bottom-details">
                                <div className="product-price">Rp. 250.000</div>
                                <div className="product-links">
                                    <a href={`/shop/1`}>View Detail<i className="fa fa-heart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="product-card">
                        <div className="badge">Discount</div>
                        <div className="product-tumb">
                            <img src="https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg" alt="product1" />
                        </div>
                        <div className="product-details">
                            <span className="product-catagory">Men's Clothing</span>
                            <h4>
                                <a href={`/shop/1`}>Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops</a>
                            </h4>
                            <p>Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday</p>
                            <div className="product-bottom-details">
                                <div className="product-price">Rp. 250.000</div>
                                <div className="product-links">
                                    <a href={`/shop/1`}>View Detail<i className="fa fa-heart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                    Loading . . .
                </div>
            </div>

            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="about">
                    <h3 className="border-bottom border-light-grey padding-16">Product Information</h3>
                </div>

                <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
                    <div className="col l3 m6 margin-bottom">
                        <div className="product-tumb">
                            <img src="https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg" alt="Product 1" />
                        </div>
                    </div>
                    <div className="col m6 margin-bottom">
                        <h3>Mens Casual Premium Slim Fit T-Shirts</h3>
                        <div style={{ marginBottom: '32px' }}>
                            <span>Category : <strong>Men's Clothing</strong></span>
                            <span style={{ marginLeft: '30px' }}>Review : <strong>4.8</strong></span>
                            <span style={{ marginLeft: '30px' }}>Stock : <strong>10</strong></span>
                            <span style={{ marginLeft: '30px' }}>Discount : <strong>0 %</strong></span>
                        </div>
                        <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                            Rp. 250.000
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            Slim-fitting style, contrast raglan long sleeve, three-button henley placket, light weight & soft fabric for breathable and comfortable wearing. And Solid stitched shirts with round neck made for durability and a great fit for casual fashion wear and diehard baseball fans. The Henley style round neckline includes a three-button placket.
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            <div><strong>Quantity : </strong></div>
                            <input type="number" className="input section border" name="total" placeholder='Quantity' />
                        </div>
                        <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                            Sub Total : Rp. 250.000
                            <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>Rp. 350.000</strong></span>
                        </div>
                        <button className='button light-grey block' >Add to cart</button>
                        <button className='button light-grey block' disabled={true}>Empty Stock</button>
                    </div>
                </div>
            </div>

            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="contact" >
                    <h3 className="border-bottom border-light-grey padding-16">Login</h3>
                    <p>Lets get in touch and talk about your next project.</p>
                    <form>
                        <input className="input border" type="text" placeholder="Username" required name="username" />
                        <input className="input section border" type="password" placeholder="Password" required name="Password" />
                        <button className="button black section" type="submit">
                            <i className="fa fa-paper-plane" />
                            Login
                        </button>
                    </form>
                </div >
            </div>
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="contact" >
                    <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
                    <p>Lets get in touch and talk about your next project.</p>
                    <form>
                        <input className="input section border" type="text" placeholder="Name" name="name" />
                        <div style={{ color: '#EF144A' }}>Name is required</div>
                        <input className="input section border" type="text" placeholder="Email" name="email" />
                        <div style={{ color: '#EF144A' }}>Email is required</div>
                        <input className="input section border" type="text" placeholder="Phone Number" name="phoneNumber" maxLength={12} />
                        <div style={{ color: '#EF144A' }}>Phone Number is required</div>
                        <input className="input section border" type="password" placeholder="Password" name="password" />
                        <div style={{ color: '#EF144A' }}>Password is required</div>
                        <input className="input section border" type="password" placeholder="Retype Password" name="retypePassword" />
                        <div style={{ color: '#EF144A' }}>Retype Password is required</div>
                        <button className="button black section" type="submit">
                            <i className="fa fa-paper-plane" /> Update
                        </button>
                    </form>
                </div>
            </div>
            <footer className="center black padding-16">
                <p>Copyright 2022</p>
            </footer>
        </>
    );
}

export default App;
